import * as React from "react";
import { Player } from "../../../models/player";
import { PlayerListItem } from "./components/player-list-item";
import { List, ListItem, ListItemText, Button } from "@material-ui/core";
import { NewPlayerDialog } from "./components/new-player-dialog";
import { PlayerRole } from "../../../models/player-role";

interface PlayerListProps {
    players: Player[];
    showDetail: Player | null;
    onPlayersUpdated: (players: Player[]) => void;
    onPlayerClick: (player: Player) => void;
}

export const PlayerList: React.FunctionComponent<PlayerListProps> = props => {
    const [showDialog, setShowDialog] = React.useState(false);

    return (
        <>
            <List>
                {props.players.map(player => (
                    <PlayerListItem
                        player={player}
                        onDelete={deleted =>
                            props.onPlayersUpdated(
                                props.players.filter(
                                    player => player !== deleted
                                )
                            )
                        }
                        onClick={player => props.onPlayerClick(player)}
                        showDetail={player === props.showDetail}
                    />
                ))}
                <ListItem alignItems="center">
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => setShowDialog(true)}
                    >
                        Spieler hinzufügen
                    </Button>
                </ListItem>
            </List>
            <NewPlayerDialog
                open={showDialog}
                onClose={() => setShowDialog(false)}
                onSubmit={name => {
                    props.onPlayersUpdated([
                        ...props.players,
                        { name, role: PlayerRole.Innocent }
                    ]);
                    setShowDialog(false);
                }}
            />
        </>
    );
};
