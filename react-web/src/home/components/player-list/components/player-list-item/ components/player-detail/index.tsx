import * as React from "react";
import { Player } from "../../../../../../../models/player";
import { getTranslation } from "../../../../../../../models/player-role";
import {
    Typography,
    Paper,
    ListItem,
    ListItemText,
    ListItemAvatar
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

interface PlayerDetailProps {
    player: Player;
}

const useStyle = makeStyles(theme => {
    return {
        detail: {
            marginLeft: theme.spacing.unit * 7
        }
    };
});

export const PlayerDetail: React.FunctionComponent<
    PlayerDetailProps
> = props => {
    const classes = useStyle();

    return (
        <ListItem>
            <ListItemText
                className={classes.detail}
                primary={getTranslation(props.player.role)}
            />
        </ListItem>
    );
};
