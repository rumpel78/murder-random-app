import * as React from "react";
import { Player } from "../../../../../models/player";
import {
    ListItem,
    ListItemAvatar,
    Avatar,
    ListItemText,
    ListItemSecondaryAction,
    IconButton
} from "@material-ui/core";
import FolderIcon from "@material-ui/icons/Folder";
import DeleteIcon from "@material-ui/icons/Delete";
import { PlayerDetail } from "./ components/player-detail";

interface PlayerListItemProps {
    player: Player;
    showDetail: boolean;
    onDelete: (player: Player) => void;
    onClick: (player: Player) => void;
}

export const PlayerListItem: React.FunctionComponent<
    PlayerListItemProps
> = props => {
    return (
        <>
            <ListItem button={true} onClick={() => props.onClick(props.player)}>
                <ListItemAvatar>
                    <Avatar>
                        <FolderIcon />
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary={props.player.name} />
                <ListItemSecondaryAction>
                    <IconButton
                        aria-label="Delete"
                        onClick={() => props.onDelete(props.player)}
                    >
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
            {props.showDetail && <PlayerDetail player={props.player} />}
        </>
    );
};
