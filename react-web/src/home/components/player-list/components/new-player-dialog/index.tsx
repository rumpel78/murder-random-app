import * as React from "react";
import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    TextField,
    DialogActions,
    Button
} from "@material-ui/core";

interface NewPlayerDialogProps {
    open: boolean;
    onClose: () => void;
    onSubmit: (name: string) => void;
}

export const NewPlayerDialog: React.FunctionComponent<
    NewPlayerDialogProps
> = props => {
    const [playerName, setPlayerName] = React.useState("");

    const handleSubmit = () => {
        props.onSubmit &&
        playerName &&
        props.onSubmit(playerName);
        setPlayerName("");
    }

    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter') {
            handleSubmit();
        }
    }

    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            aria-labelledby="form-dialog-title"
        >
            <DialogTitle id="form-dialog-title">Spieler hinzufügen</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Gib bitte den Namen des neuen Spielers ein
                </DialogContentText>
                <TextField
                    autoFocus={true}
                    margin="dense"
                    id="name"
                    label="Name"
                    fullWidth={true}
                    value={playerName}
                    onChange={event => setPlayerName(event.currentTarget.value)}
                    onKeyPress={handleKeyPress}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onClose} color="primary">
                    Abbrechen
                </Button>
                <Button
                    onClick={() => handleSubmit()}
                    color="primary"
                >
                    Hinzufügen
                </Button>
            </DialogActions>
        </Dialog>
    );
};
