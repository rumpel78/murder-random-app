import Fab from "@material-ui/core/Fab";
import SwapIcon from "@material-ui/icons/SwapHoriz";
import * as React from "react";
import { makeStyles } from "@material-ui/styles";
import { PlayerList } from "./components/player-list";
import { PlayerRole } from "../models/player-role";
import { Player } from "../models/player";
import { defaultPlayers } from "../models/default-players";

const useStyles = makeStyles(theme => {
    return {
        container: {
            height: "100%"
        },
        fab: {
            position: "absolute",
            right: theme.spacing.unit,
            bottom: theme.spacing.unit
        },
        extendedIcon: {
            marginRight: theme.spacing.unit
        }
    };
});

export const Home: React.FunctionComponent = () => {
    const initialPlayers = () => {
        const savedPlayers = window.localStorage.getItem("players");
        if (savedPlayers) {
            return savedPlayers.split("|").map<Player>(name => ({
                name: name,
                role: PlayerRole.Innocent
            }));
        }
        return defaultPlayers;
    };

    const classes = useStyles();
    const [showDetail, setShowDetail] = React.useState<Player | null>(null);
    const [players, setPlayers] = React.useState(initialPlayers);

    React.useEffect(() => {
        window.localStorage.setItem(
            "players",
            players.map(player => player.name).join("|")
        );
    }, [players]);

    const randomizePlayers = () => {
        const playerCount = players.length;
        if (playerCount < 3) {
            return;
        }

        let sheriff = 0;
        const murder = Math.floor(Math.random() * playerCount);
        do {
            sheriff = Math.floor(Math.random() * playerCount);
        } while (murder === sheriff);

        players.forEach(player => (player.role = PlayerRole.Innocent));
        players[murder].role = PlayerRole.Murder;
        players[sheriff].role = PlayerRole.Sheriff;
        setPlayers(players);
        setShowDetail(null);
    };

    return (
        <div className={classes.container}>
            <PlayerList
                players={players}
                showDetail={showDetail}
                onPlayersUpdated={players => setPlayers(players)}
                onPlayerClick={player =>
                    setShowDetail(player === showDetail ? null : player)
                }
            />
            <Fab
                color="primary"
                aria-label="Add"
                className={classes.fab}
                disabled={players.length < 3}
                onClick={() => randomizePlayers()}
            >
                <SwapIcon />
            </Fab>
        </div>
    );
};
