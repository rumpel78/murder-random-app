import React, { Component } from "react";
import { Home } from "./home";
import { createMuiTheme, CssBaseline } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";

const theme = createMuiTheme();

class App extends Component {
    render() {
        return (
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <Home />
            </ThemeProvider>
        );
    }
}

export default App;
