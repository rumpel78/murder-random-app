export enum PlayerRole {
    Innocent,
    Murder,
    Sheriff
}

export const getTranslation = (role: PlayerRole) => {
    switch (role) {
        case PlayerRole.Innocent:
            return "Innocent";
        case PlayerRole.Murder:
            return "Murder";
        case PlayerRole.Sheriff:
            return "Sheriff";
    }
};
