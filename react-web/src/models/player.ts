import { PlayerRole } from "./player-role";

export interface Player {
    name: string;
    role: PlayerRole;
}
