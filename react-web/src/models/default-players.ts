import { Player } from "./player";
import { PlayerRole } from "./player-role";

export const defaultPlayers: Player[] = [
    { name: "Kevin", role: PlayerRole.Innocent },
    { name: "Justin", role: PlayerRole.Innocent },
    { name: "Ricardo", role: PlayerRole.Innocent }
];
