import * as React from 'react';
import { WebView } from 'react-native';

export default class App extends React.Component {

  render() {
    return (
      <WebView
        source={{uri: 'https://aaron.rzipa.at'}}
        style={{marginTop: 20}}
      />
    );
  }
}

